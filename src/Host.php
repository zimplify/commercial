<?php
    namespace Zimplify\Commercial;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Commercial\{Enterprise, Staff};
    use Zimplify\Security\Agent;
    use \DateTime;
    use \RuntimeException;
    
    /**
     * a Host is the company that offer this application to the customers
     * @package Zimplify\Commercial (code 06)
     * @type Instance (code 01)
     * @file Host (code 02)
     */
    class Host extends Enterprise {

        const CLS_STAFF = "core-comm::staff";
        const ERR_DUPLICATE_DETECTED = 500060102001;

        /**
         * elevate a user to administration privilege. the current admin will no longer have this right after this.
         * @param Agent $agent the agent to allow admin status.
         * @return Agent
         */
        public function elevate(Agent $agent) : Agent {
            $result = parent::elevate($agent);
            $result->roles = "systemwide.all";
            return $result;
        }

        /**
         * the override version of the prepare()
         * @return void
         */
        protected function prepare() {         
        }

        /**
         * the override version of the presave()
         * @return void
         */        
        protected function presave() {
            parent::presave();
            if (count(Application::search([Query::SRF_TYPE => $this->type])) > 0) 
                throw new RuntimeException("Only one host can exist in this application.", self::ERR_DUPLICATE_DETECTED);
        }

        /**
         * get the default agent types within the organization
         * @return Agent
         */
        public function resident() : Agent {
            return Application::create(self::CLS_STAFF, $this);
        }

        /**
         * create data summary on objects in system
         * @param string $type the type reference in the system
         * @return array
         */
        public function summarize(string $type) : array {

        }
        
    }