<?php
    namespace Zimplify\Commercial\Requests;
    use Zimplify\Core\{Application, Document, Event, Query};
    use Zimplify\Core\Interfaces\{ISpawnableInterface, IWorkflowCapableInterface};
    use Zimplify\Commercial\Enterprise;
    use \RuntimeException;

    /**
     * this is to enable registering enterprises onto the application
     * @package Zimplify\Commercial (code 06)
     * @type Document (code 02)
     * @file Enrollment (code 03)
     */
    class Enrollment extends Document implements ISpawnableInterface, IWorkflowCapableInterface {

        /**
         * creating a new enterprise instance based on the request needs
         * @return void
         */
        public function create() : Enterprise {

            // creating our data set
            $data = [];
            $data[self::FLD_NAME] = $this->name;
            $data[self::FLD_ADDRESS] = $this->address;
            $data[self::FLD_REGISTRATION] = $this->registration;

            // now create the instance and return
            $result = (Application::create($type, null, $data))->save();
            $result->record($this->generate($result));

            // return the result
            return $result;
        }

        /**
         * generate an event for creating the company
         * @return Event
         */
        protected function generate(Enterprise $company) : Event {
            // generate the data set
            $data = [];
            $data["event"] = "company.create";
            $data["source"] = $this->id;
            $data["description"] = "Created company on application.";
            $data["reference"] = $company->id;

            // now creating the instance
            return Application::create(self::CLS_EVENT, $company, $data);
        }

        /**
         * override for initializing function
         * @return void
         */
        protected function prepare() {
        }

    
    }
    