<?php
    namespace Zimplify\Commercial\Requests;
    use Zimplify\Core\Document;
    use Zimplify\Core\Interfaces\IPauseableInterface;
    use Zimplify\Core\Interfaces\IWorkflowCapableInterface;

    /**
     * the request for forcing reset on authentication for an agent
     * @package Zimplify\Commercial (code 06)
     * @type Document (code 02)
     * @file Reset (code 02)
     */
    class Reset extends Document implements IPauseableInterface, IWorkflowCapableInterface {

        const ERR_BAD_CONFIRMATION = 403060202001;
        const ERR_BAD_REPLY = 400060202002;
        const PDR_WORKFLOW = "core::workflow";
        const PRM_CONFIRMATION = "confirmation";
        const PRM_SECURET = "secret";

        /**
         * trigger the document to continue processing
         * @param array $inputs (optional) the inputs returned to the workflow
         * @return mixed
         */
        public function continue(array $inputs = []) {
            if (array_key_exists(self::PRM_CONFIRMATION, $inputs) && array_key_exists(self::PRM_SECURET, $inputs)) 
                if ($inputs[self::PRM_CONFIRMATION] == $this->confirmation) {
                    $adapter = Application::request(self::PDR_WORKFLOW, []);
                    return $adapter->run($this, "main", $inputs);
                } else
                    throw new RuntimeException("Failed to match confirmation code.", self::ERR_BAD_CONFIRMATION);                
            else 
                throw new RuntimeException("Failed to commit request", self::ERR_BAD_REPLY);
        }

        /**
         * the preparation steps of the instance during initialization
         * @return void
         */        
        protected function prepare() {
        }

        /**
         * overriding the presave routine
         * @return void
         */
        protected function presave() {
            parent::presave();

            // adding the confirmation code
            $this->confirmation = rand(10000, 99999);
        }
        
        /**
         * complete the actual reset operation
         * @param string $secret the secret to update
         * @return bool
         */
        public function reset() : bool {
            return $this->parent()->reset($this->secret) ? true : false;
        }
    }