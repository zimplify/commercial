<?php
    namespace Zimplify\Commercial;
    use Zimplify\Security\Agent;
    use Zimplify\Security\Interfaces\{ITokenProviderInterface, IReinstallableInterface};

    /**
     * a Consumer is the end user that access the application via apps
     * @package Zimplify\Commercial (code 06)
     * @type Instance (code 01)
     * @file Consumer (code 03)
     */    
    class Consumer extends Agent implements IReinstallableInterface, ITokenProviderInterface {

        /**
         * generating a new token for headers
         * @param string $device the device type when accessing
         * @param string $client the source address
         * @return string
         */
        public function generate(string $device, string $client) : string {
            $expiry = (new DateTime())->add(new DateInterval("P30D"));
            $data = [self::HDF_OBJECT => $this->id, self::HDF_DEVICE => $device , self::HDF_ADDRESS => $client, self::HDF_EXPIRY => $expiry->format("U")];
            return Application::request(self::PDR_SECURE_TOKEN, [])->encode($data);
        }        

        /**
         * handling the actions we need to do before deleting the account.
         * @return void
         */
        protected function revoke() {
        }                
    }