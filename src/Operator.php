<?php
    namespace Zimplify\Commercial;
    use Zimplify\Core\Interfaces\IAuthorInterface;
    use Zimplify\Security\Interfaces\{ITokenProviderInterface, IReinstallableInterface};
    use Zimplify\Security\{Agent};

    /**
     * a Operator is an agent that a Host will generate for operations
     * @package Zimplify\Commercial (code 06)
     * @type Instance (code 01)
     * @file Operator (code 05)
     */
    class Operator extends Agent implements IAuthorInterface, IReinstallableInterface {

        const DEF_CLS_NAME = "Zimplify\\Commercial\\Operator";
        const DEF_SHT_NAME = "core-comm::operator";
        
        /**
         * check if this operator an administrator to manage settings
         * @return bool
         */
        public function isAdministrator() : bool {
            return $this->{self::FLD_ASSIGNED_ADMIN} ?? false;
        }        

        /**
         * generating a new token for headers
         * @param string $device the device type when accessing
         * @param string $client the source address
         * @return string
         */
        public function generate(string $device, string $client) : string {
            $expiry = (new DateTime())->add(new DateInterval("P30D"));
            $data = [self::HDF_OBJECT => $this->id, self::HDF_DEVICE => $device , self::HDF_ADDRESS => $client, self::HDF_EXPIRY => $expiry->format("U")];
            return Application::request(self::PDR_SECURE_TOKEN, [])->encode($data);
        }

        /**
         * handling the actions we need to do before deleting the account.
         * @return void
         */
        protected function revoke() {
        }        
    }