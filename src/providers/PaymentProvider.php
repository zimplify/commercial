<?php
    namespace Zimplify\Commercial\Providers;
    use Zimplify\Core\{Application, Provider};
    use \RuntimeException;

    /**
     * this provider offer a multi-provider multiplexer in charging
     * @package Zimplify\Commercial (code 06)
     * @type Provider (code 03)
     * @file PaymentProvider (code 01)
     */
    class PaymentProvider extends Provider {

        const CFG_CLIENT_OPTIONS = "system.providers.payment.options";
        const ERR_NO_ADAPTER = 404060301001;

        private $clients = [];

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {            
            // loading out the initialized configuration
            foreach (Application::env(self::CFG_CLIENT_OPTIONS) as $option => $setup) 
                if (array_key_exists(self::FLD_ENGINE, $setup) && ($adapter = Application::request($setup[self::FLD_ENGINE], [])))
                    $this->clients[$option] = $adapter;            
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */        
        protected function isRequired() : bool {
            return true;
        }

        /**
         * issuing the charge toward the provider
         * @param string $asset the reference for charging in the payment system (account/wallet id, etc)
         * @param float $amount the amount to charge
         * @param string $currency (optional) the currency of the charge
         * @param string $option (optional) the method to charge
         * @return string
         */
        public function charge(string $asset, float $amount, string $curreny = "hkd", string $option = "credit") : string {
            if (array_key_exists($option, $this->clients)) {
                $result = $this->clients[$option]->charge($asset, $amount, $currency);
                return $result;
            } else
                throw new RuntimeException("The requested adapter $option is not available.", self::ERR_NO_ADAPTER);
        }
    }