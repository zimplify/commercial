<?php
    namespace Zimplify\Commercial;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Commercial\Enterprise;
    use Zimplify\Commercial\Interfaces\{IBillableInterface, IChargeableInterface};
    use \Datetime;
    use \RuntimeException;

    /**
     * the Subscriber represent enterprise that subscribes to service contracts within the application
     * @package Zimplify\Commercial (code 06)
     * @type Instance (code 01)
     * @file Subscriber (code 04)
     */
    class Subscriber extends Enterprise implements IChargeableInterface {

        const CFG_CONTRACT_TYPES = "application.operations.contract-types";
        const CLS_INVOICE = "core-comm::invoice";
        const CLS_STAFF = "core-comm::operator";
        const FLD_ADMIN = "admin";
        const FLD_CONTRACTS = "contracts";
        const FLD_INVOICES = "invoices";
        const FLD_LIAISON = "liaison";
        const FLD_STAFF = "operators";

        /**
         * our magic override
         * @param string $param the parameter to read
         * @return mixed
         */
        public function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::FLD_ADMIN: 
                    $search = Application::search([Query::SRF_TYPE => self::CLS_STAFF, Query::SRF_OWNER => $this->id, "isAdmin" => true]);
                    $result = count($search) > 0 ? $search[0] : null;
                    break;
                case self::FLD_CONTRACTS: 
                    $result = $this->commitments();
                    break;
                case self::FLD_INVOICES:
                    $result = $this->charged();
                    break;
                case self::FLD_LIAISON:
                    $result = $this->manager ? Application::load($this->manager, self::CLS_LIAISON) : null;
                    break;
                case self::FLD_STAFF:
                    $result = Application::search([Query::SRF_TYPE => self::CLS_STAFF, Query::SRF_OWNER => $this->id, Query::SRF_STATUS => true]);
                    break;
                default: 
                    $result = parent::__get($param);
            }
            return $result;
        }

        /**
         * perform the charge onto the instance
         * @param IBillableInterface $invoice the reference to charge
         * @return string
         */
        public function charge(IBillableInterface $invoice) : string {
            // load the details out from invoice
            $amount = $invoice->total(); 
            $currency = $invoice->{IBillableInterface::FLD_CURRENCY};

            // now we fire the request out
            return Application::request(self::PDR_PAYMENT, [])
                ->charge(
                    $invoice->customer()->{self::FLD_CHARGE_RECORD}, 
                    $amount, 
                    $currency, 
                    $invoice->customer()->{self::FLD_CHARGE_METHOD});
        }

        /**
         * get the list of invoices paid by this enterirse
         * @return array
         */
        public function charged(int $from = null) : array {
            return Application::search([Query::SRF_OWNER => $this->id, Query::SRF_TYPE => self::CLS_INVOICE]);
        }

        /**
         * the commitments current active for the instance
         * @param bool $active (optional) only find the active contracts
         * @return array
         */
        public function commitments(bool $active = true) : array {
            $result = [];

            // now look through all types
            foreach (Application::env(self::CFG_CONTRACT_TYPES) ?? [] as $type) {
                $search = Application::search([Query::SRF_TYPE => $type, Query::SRF_OWNER => $this->id, Query::SRF_STATUS => $active]);
                if (count($search) > 0) 
                    $result = array_merge($result, $search);
            }

            return $result;
        }

        /**
         * get the default agent types within the organization
         * @return Agent
         */
        public function resident() : Agent {
            return Application::create(self::CLS_STAFF, $this);
        }
    }