<?php
    namespace Zimplify\Commercial;
    use Zimplify\Core\Interfaces\IAuthorInterface;
    use Zimplify\Security\Interfaces\{ITokenProviderInterface};
    use Zimplify\Security\{Agent};

    /**
     * a Staff is an agent that a Host will generate for operations
     * @package Zimplify\Commercial (code 06)
     * @type Instance (code 01)
     * @file Host (code 03)
     */
    class Staff extends Agent implements IAuthorInterface {

        const DEF_CLS_NAME = "Zimplify\\Commercial\\Staff";
        const DEF_ADMIN_ROLE = "internal.admin";

        /**
         * check if the staff is an administrator for the Host
         * @return bool
         */
        public function isAdmin() : bool {
            return $this->admin;
        }

        /**
         * generating a new token for headers
         * @param string $device the device type when accessing
         * @param string $client the source address
         * @return string
         */
        public function generate(string $device, string $client) : string {
            $expiry = (new DateTime())->add(new DateInterval("P7D"));
            $data = [self::HDF_OBJECT => $this->id, self::HDF_DEVICE => $device , self::HDF_ADDRESS => $client, self::HDF_EXPIRY => $expiry->format("U")];
            return Application::request(self::PDR_SECURE_TOKEN, [])->encode($data);
        }

        /**
         * the override presave routine for the instance
         * @return void
         */
        protected function presave() {
            parent::presave();

            // making sure the admin related matter is taken care of
            $roles = $this->{self::FLD_ROLES};
            if ($this->admin) 
                array_push($roles, self::DEF_ADMIN_ROLE);            
        }

        /**
         * handling the actions we need to do before deleting the account.
         * @return void
         */
        protected function revoke() {
        }                
    }