<?php
    namespace Zimplifu\Commercial\Middlewares;
    use Zimplify\Commercial\Staff;
    use Zimplify\Core\Services\ClassUtils;
    use Zimplifu\Security\Middlewares\AgentValidationMiddleware;

    /**
     * this middleware aims to check for staff in token and make sure it is provided
     * @package Zimplify\Security (code 02)
     * @type middleware (code 10)
     * @file StaffDetectMiddleware (code 02)
     */
    class StaffDetectMiddleware extends AgentValidationMiddleware {

        /**
         * make sure the agnet is of the right privilege
         * @param Agent $agent the detected agent
         * @return bool
         */
        protected function validate(Agent $agent) : bool {
            return ClassUtils::is($agent, Staff::DEF_CLS_NAME);
        }        
    }