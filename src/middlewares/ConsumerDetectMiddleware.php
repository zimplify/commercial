<?php
    namespace Zimplifu\Commercial\Middlewares;
    use Zimplify\Commercial\Consumer;
    use Zimplify\Core\Services\ClassUtils;
    use Zimplifu\Security\Middlewares\AgentValidationMiddleware;

    /**
     * this middleware aims to check for consume in token and make sure it is provided
     * @package Zimplify\Security (code 02)
     * @type middleware (code 10)
     * @file ConsumerDetectMiddleware (code 04)
     */
    class ConsumerDetectMiddleware extends AgentValidationMiddleware {

        /**
         * make sure the agnet is of the right privilege
         * @param Agent $agent the detected agent
         * @return bool
         */
        protected function validate(Agent $agent) : bool {
            return ClassUtils::is($agent, Consumer::DEF_CLS_NAME);
        }        
    }