<?php
    namespace Zimplify\Commercial\Interfaces;
    use Zimplify\Commercial\Interfaces\IBillableInterface;

    /**
     * The interface to indicate whether the instance can be charged
     * 
     * @package Zimplify\Commercial (code 06)
     * @type Interface (code 06)
     * @file IChargeableInterface (code 02)
     */
    interface IChargeableInterface {

        const CFG_ALLOW_OVERDRAFT = "application.billing.overdraft";
        const FLD_CHARGE_METHOD = "charging.method";
        const FLD_CHARGE_RECORD = "charging.record";        
        
        /**
         * perform the charge onto the instance
         * @param IBillableInterface $invoice the reference to charge
         * @return string
         */
        function charge(IBillableInterface $invoice) : string;

        /**
         * get the list of invoices paid by this enterirse
         * @return array
         */
        function charged() : array;

        /**
         * the commitments current active for the instance
         * @return array
         */
        function commitments() : array;
        
    }