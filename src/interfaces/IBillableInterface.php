<?php
    namespace Zimplify\Commercial\Interfaces;
    use Zimplify\Commercial\Enterprise;

    /**
     * The interface to indicate whether the instance contains billing information
     * 
     * @package Zimplify\Commercial (code 06)
     * @type Interface (code 06)
     * @file IBillableInterface (code 01)
     */
    interface IBillableInterface {

        const FLD_CURRENCY = "currency";
        const FLD_CUSTOMER = "customer";

        /**
         * identify the party to be charged
         * @return Enterprise
         */
        function customer() : Enterprise;

        /**
         * calculate the total of the instance
         * @return float
         */
        function total() : float;
    }