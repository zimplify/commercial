<?php
    namespace Zimplify\Commercial;
    use Zimplify\Commercial\Operator;
    use Zimplify\Core\{Application, Instance, Query};
    use Zimplify\Core\Interfaces\ITraceableInterface;
    use Zimplify\Security\Agent;
    use \RuntimeException;

    /**
     * Enterprise is the base for companies that interact within the application
     * @package Zimplify\Commercial (code 06)
     * @type Instance (code 01)
     * @file Enterprise (code 01)
     */
    abstract class Enterprise implements ITraceableInterface {

        const ERR_DUPLICATE_ORG = 500060101001;
        const FLD_ADMIN = "admin";
        const FLD_AGENTS = "agents";               
        const FLD_EXPIRY_MONTH = "registration.expiry.month";
        const FLD_EXPIRY_YEAR = "registration.expiry.year";
        const FLD_IS_ADMIN = "isAdmin";
        const FLD_NAME = "string";
        const FLD_SERIAL = "registration.serial";

        /**
         * the override version of the __get magic method
         * @param string $param the parameter to read from
         * @return mixed
         */
        public function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::FLD_ADMIN: 
                    $result = Application::search([Query::SRF_TYPE => $this->resident(), Query::SRF_OWNER => $this->id, self::FLD_IS_ADMIN => true]);
                    $result = count($result) > 0 ? $result[0] : null;
                    break;                    
                case self::FLD_AGENTS:
                    $result = Application::search([Query::SRF_TYPE => $this->resident(), Query::SRF_OWNER => $this->id]);
                    break;
                default: 
                    $result = parent::__get($param);
            }
            return $result;
        }

        /**
         * elevate a user to administration privilege. the current admin will no longer have this right after this.
         * @param Agent $agent the agent to allow admin status.
         * @return Agent
         */
        public function elevate(Agent $agent) : Agent {
            // try to see if we have an admin
            if ($admin = $this->admin) {
                $admin->{self::FLD_IS_ADMIN} = false;
                $admin->roles = null;
                $admin->save();
            }

            // now do the real work
            $agent->{self::FLD_IS_ADMIN} = true;
            return $agent;
        }

        /**
         * inspect whether the enterprise can classify as active
         * @return bool
         */
        protected function inspect() : bool {
            $now = new DateTime();
            $valid =  $now->format("Y") < $this->{self::FLD_EXPIRY_YEAR} || 
                ($now->format("Y") == $this->{self::FLD_EXPIRY_YEAR} && $now->format("m") < $this->{self::FLD_EXPIRY_MONTH});

            // add any additional checks here if any
            if ($valid) {
                return true;
            } else 
                return false;
        }

        /**
         * create a new agent within the organization
         * @param array $data the registration data for the enterprise
         * @return mixed
         */
        public function join(array $contact, Agent $author, array $roles = null) {
            $registration = Application::create(self::DOC_REGISTRATION, $this);

            // making rhe dasta
            $data = ["contact" => $contact];
            if ($roles) $data["roles"] = $roles;

            // populate the data
            $registration->populate($data)->authored($author)->save();
            
            // now we run the request
            $result = Application::request(self::PDR_WORKFLOW, [])->run($registration);
            return $result;
        }

        /**
         * the overriding presave function
         * @return void
         */
        protected function presave() {
            parent::presave();
            if (count(Application::search([Query::SRF_TYPE => $this->type, self::FLD_SERIAL => $this->{self::FLD_SERIAL}, Query::SRF_STATUS => true])) > 0) 
                throw new RuntimeException("Orgnization already exists.", self::ERR_DUPLICATE_ORG);
        }

        /**
         * record a new event into the instance
         * @param Event $event the event to record
         * @return ITraceableInterface
         */
        public function record(Event $event) : self {
            return $this;            
        }

        /**
         * get the default agent types within the organization
         * @return string
         */
        public function resident() : string {
            return Operator::DEF_SHT_NAME;
        }

        /**
         * remove the agent off from the organization
         * @param string $agent the agent to remove
         * @param Agent $replace the agent to replace the current task assigned to the removing agent
         * @return bool
         */
        public function unjoin(string $agent, Agent $replace = null) : bool {
        }
    }