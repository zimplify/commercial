<?xml version="1.0" encoding="UTF-8"?>
<data name="address">
    <data name="street" type="text" />
    <data name="suburb" type="text" />
    <data name="city" type="text" />
    <data name="state" type="text" />
    <data name="country" type="text" />
    <data name="postcode" type="text" />
</data>